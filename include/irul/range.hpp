#ifndef IRUL_RANGE_HPP
#define IRUL_RANGE_HPP

#include <cstddef>
#include <iterator>

namespace irul {

// The second argument must not be provided explicitly, it is only used to
// select the constness of the range
template<typename Iterator, typename ValueType = typename std::iterator_traits<Iterator>::value_type>
class range {
public:
	using value_type = ValueType;

	range(Iterator first, Iterator last): m_begin{first}, m_end{last} {}

	Iterator begin() {return m_begin;}
	Iterator end() {return m_end;}

	// This won't be an error for weaker iterators until use,
	// so there is no need to use hacks like enable_if to
	// remove it:
	std::size_t size() {
		return static_cast<std::size_t>(m_end - m_begin);
	}
private:
	Iterator m_begin;
	Iterator m_end;
};

// specialization for const iterators:
template<typename Iterator, typename ValueType>
class range<Iterator, const ValueType> {
public:
	using value_type = const ValueType;

	range(Iterator first, Iterator last): m_begin{first}, m_end{last} {}

	Iterator begin() const {return m_begin;}
	Iterator end() const {return m_end;}
	Iterator cbegin() const {return m_begin;}
	Iterator cend() const {return m_end;}

	std::size_t size() {
		return static_cast<std::size_t>(m_end - m_begin);
	}
private:
	Iterator m_begin;
	Iterator m_end;
};

} // namespace irul

#endif
