IteratoRUtiLs - Making writing iterators less painfull
======================================================

Introduction
------------

While working with iterators is (relatively) pleasent as a user in C++, writing them
can be quite painfull because of the tons of boilerplate that they require. To make
matter even more annoying, most of that boilerplate is pretty much redundant.

IRUL attempts to fix that by providing a few small mixins that allow the creation of
complete iterators by implementing only the bare minimum of functionality: In no case
is the implementation of more than four methods required that additionaly have speaking
names like “next”, “get”, “advance” or “compare”.

In addition to that the code of IRUL itself is very tame in terms of complexity when
compared to other template-libraries. The by far most advanced idiom is in fact
[CRTP](https://en.wikipedia.org/wiki/Curiously_recurring_template_pattern), which is
probably the easiest nontrivial template-idiom that exists.

Finally IRUL comes without dependencies: A somewhat conforming C++11-compiler
with it's stdlib should be enough to use it. (The unit-tests require Boost::test
in addition to that, but building them is strictly optional.)

Usage
-----

To create a new iterator one must create a new class that inherits from the appropriate
IRUL-mixin that in return recieves the iterator-type and it's value-type as template-parameters.

An example will probably be the easiest way to grasp that:

```cpp
#include <irul/irul.hpp>

// Note the first template-parameter: This is the CRTP, that I mentioned:
class integer_iterator: public irul::input_iterator<integer_iterator, const int> {
	// methods...
};
```

This is not completely unlike the use of `std::iterator` except that IRUL doesn't forget
to implement the actually worst part of the boilerplate as well.

Another thing that is similar to `std::iterator` is the way that you annotate an iterator
as a const-iterator by passing `const T` instead of `T` as the value-type-parameter.

Iterator-requirements
---------------------

So, what are the methods that are required? This depends on the iterator you want.
It should be noted that a more powerfull iterator doesn't necessarily require all
the methods that a weaker one needs; bidirectional iterators do in fact need one
more method than random-access-iterators.

### Input-iterators

Input iterators require three methods: One to get the referenced value, one to
increment it and one to compare it. Assuming that `It` is the type of the iterator and
`Ref` the iterators reference-type (by default: a normal reference to the value-type,
though this can be changed), they must have a form that is compatible to these:

* `Ref It::get() const`
* `void It::next()`
* `bool It::equal(It) const`
* (Appropriate constructors, assignment-operators, a destructor,…; the defaults will do here!)

For instance: The above mentioned `integer_iterator` can be implemented like this:

```cpp
#include <irul/irul.hpp>

class integer_iterator: public irul::input_iterator<integer_iterator, const int> {
public:
	integer_iterator(int val = 0): m_val{val} {}

	const int& get() const {return m_val;}
	void next() {++m_val;}
	bool equal(integer_iterator other) const {return m_val == other.m_val;}
private:
	int m_val;
};
```

### Forward-iterators

Forward iterators are interessting in that they don't require any more methods than
input-iterators, only stricter guarantees regarding iterator-invalidation: If you
are able to make a copy of an input-iterator, increment it a few times and are then
able to receive the exact same sequence of values from the original iterator, you
should be able to change it into a forward-iterator.

For instance that is the case with our integer-iterator, so we can improve it by just
replacing the term `input_iterator` in the above code with `forward_iterator`.

### Bidirectional-iterators

The one important difference between forward-iterators and bidirectional-iterators is that
the later can go to the previous element. With IRUL we need one more method for that:

* `void It::prev()`

It should be pretty obvious what it does, and may look like this for out integer-iterator:

```cpp
void prev() {--m_val}
```

After adding that we can update it to a bidirectional-iterator by replacing the token
`forward_iterator` with `bidirectional_iterator`.

### Random-Access-Iterators

This leads us to the most powerfull iterator-category: Random-access-iterators.

In addition to bidirectional iterators those can jump an arbitrary distance in both
directions and are capable of telling us the distance between them, which also enables
them to provide us with a total ordering of them via the comparission-operators.

To implement all those things IRUL requires three methods:

* `Ref It::get() const` (we already know this one)
* `void It::advance(Dist)` (replacing both `next` and `prev`)
* `Dist It::distance(It other) const`

In this case `Dist` is the difference-type of the iterator which defaults to `std::ptrdiff_t`
but may be customized.

Note that `advance` must accept negative values as arguments and work fine with them.

The distance between `*this` and `other` must be returned similar to they
way that `std::distance` works: if `*this` comes before `other`, the value must be
positive, and if it comes after `other` it must be negative.

Again, integer-iterator can be implemented as a random-access-iterator by inheriting from
`random_access_iterator`:

```cpp
class integer_iterator : public irul::random_access_iterator<int_iterator, const int> {
public:
	integer_iterator(int val = 0) : m_val{val} {}

	void advance(std::ptrdiff_t dist) { m_val += static_cast<int>(dist); }
	const int& get() const { return m_val; }
	std::ptrdiff_t distance(const int_iterator& other) const { return other.m_val - m_val; }

private:
	int m_val;
};
```


### Output-iterators

Output iterators are somewhat weird even in the standard-library, so implementing
them with IRUL is also untypical: As usual we inherit from the appropriate mixin,
but this time we only provide the iterator-type (`output_iterator`) as argument,
not the value-type. In fact `output_iterator` only takes one template-argument.

The we will have to import the assignment-operator from it with a using-stament
and implement `set()`-methods for all types that we are willing to accept. This
may for example look like this:

```cpp
class printer_iterator : public irul::output_iterator<printer_iterator> {
public:
	// this line is important:
	using output_iterator::operator=;
	
	// you can overload it, provide it as template, …
	// everything like that works here:
	template <typename T>
	void set(const T& arg) {
		std::cout << arg;
	}
};
```

The only important part about your `set`-methods is that they work when receiving
one argument. `output_iterator` will perfectly forward everything that is written
to it to your class as if calling `it.set(expression)` where `it` is an instance
of the iterator-class. Note that temporaries are forwarded as temporaries so
you might be able to profit performance-wise by accpeting them as that instead
of just taking const-references.

It should be noted that `set`-methods that take a different amount of arguments
are no problem in and of themselves, they will just be ignored.


### Customizing iterator-details

All input-iterator-mixins (=all, except for `output_iterator`), take five
template-arguments of which the first two must be provided. The other three are,
in that order:

* `Distance` (default: `std::ptrdiff_t`): This will be used as `difference_type`. Other
  signed integers should work as well.
* `Pointer` (default: `ValueType*`): The pointer-type that the iterator uses. Only
  change this if you know what you are doing!
* `Reference` (default: `ValueType&`): The pointer-type that the iterator uses.
  Only change this is you **really** know what you are doing!

As a good first approximation, you should probably not have to touch those and
are unlikely to gain real benefits from doing so. They are provided mostly for
completeness and those who understand all the implications. Since there are
currently no tests for using this feature it basically boils down to
“Here be dragons”.

### Ranges

IRUL comes with a very basic range-type that is basically just a wrapper around
a pair of iterators. An example will probably demonstrate the use best:

```cpp
#include <irul/range.hpp>

// ...

int main() {
	using myrange = irul::range<std::istream_iterator<int>>;
	myrange rng{{std::cin}, {}};
	for(auto val: rng) {
		use(val);
	}
}
```

Basically `irul::range` takes the iterator-type as an argument and can then
be constructed from two iterators of that type. It will then provide a `begin()`
and an `end()`-method that return the respective iterators. If the iterator is
a const iterator those methods are marked as const and `cbegin()` and
`cend()`-methods with the same semantics are provided. Otherwise those methods
will not be const and `cbegin()` and `cend()` are not provided.

In addition to that there is a `size()`-method that will return the distance
between two random-access-iterators and a compilation-error upon use if no
subtraction between the two iterators is defined.

Finally there is a `value_type`-typedef that is basically just the value-type
of the iterator.