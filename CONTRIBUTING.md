Ways to contribute
==================

Simple and little work
----------------------

* File bug-reports if you find bugs
* File feature-requests (but think about whether what you want should really be here)

More advanced and with varying amounts of work
---------------------------------------

* Provide new testcases
* Provide new demos
* Extend the documentaion
* Search and fix bugs and suboptimal code

Coding Standards
================

You can find a first overview of my coding-standards (that are used in this project)
on my private [homepage](http://florianjw.de/en/cpp_coding_standard.html) ([CAcert signed SSL-version](https://florianjw.de/en/cpp_coding_standard.html)).

Licensing
=========

All significant additions have to be provided under the LGPLv3.

For tiny additions (fixed typos, cosmetic stuff, …) I require them to be
provided under public domain or similar terms in order to keep the number of
people who have legal contoll over the code managable.


