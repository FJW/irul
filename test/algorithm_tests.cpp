#include <vector>
#include <algorithm>

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "irul/irul.hpp"

namespace {
	struct ra_test_it: public irul::random_access_iterator<ra_test_it, int> {
		ra_test_it(std::vector<int>::iterator it, std::vector<int>& vec)
		        : m_it{it}, m_vec{&vec} {}
		int& get() const { return *m_it; }
		void advance(std::ptrdiff_t dist) { m_it += dist; }
		std::ptrdiff_t distance(ra_test_it other) const {
			return std::distance(m_it, other.m_it);
		}
		std::size_t index() const {
			return static_cast<std::size_t>(std::distance(m_vec->begin(), m_it));
		}
		std::vector<int>::iterator m_it;
		std::vector<int>* m_vec;
	};

	struct in_test_it: public irul::input_iterator<in_test_it, const int> {
		in_test_it(int val = 0): m_val{val} {}
		const int& get() const {return m_val;}
		void next() {++m_val;}
		bool equal(in_test_it other) const {return m_val == other.m_val;}
		int m_val;
	};

}

BOOST_AUTO_TEST_SUITE( Algorithm_tests )

BOOST_AUTO_TEST_CASE( sort ) {
	auto vec = std::vector<int>{2, 1, 5, 5, 2, 3, 7, 3};
	auto it1 = ra_test_it{vec.begin(), vec};
	auto it2 = ra_test_it{vec.end(), vec};
	std::sort(it1, it2);
	BOOST_CHECK(std::is_sorted(vec.begin(), vec.end()));
	BOOST_CHECK_EQUAL(std::distance(it1, it2), vec.size());
	BOOST_CHECK(std::equal(vec.begin(), vec.end(), it1));
}

BOOST_AUTO_TEST_CASE( fill_n ) {
	auto vec = std::vector<int>{};
	std::copy_n(in_test_it{}, 5, std::back_inserter(vec));
	BOOST_CHECK(vec == (std::vector<int>{0,1,2,3,4}));
}

BOOST_AUTO_TEST_SUITE_END()
