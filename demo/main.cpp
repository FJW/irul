
#include "irul/irul.hpp"

#include <algorithm>
#include <iostream>
#include <string>
#include <vector>

class int_iterator : public irul::random_access_iterator<int_iterator, const int> {
public:
	int_iterator(int val) : m_val{val} {}

	void advance(std::ptrdiff_t dist) { m_val += static_cast<int>(dist); }
	const int& get() const { return m_val; }
	std::ptrdiff_t distance(const int_iterator& other) const { return other.m_val - m_val; }

private:
	int m_val;
};

class printer_iterator : public irul::output_iterator<printer_iterator> {
public:
	using output_iterator::operator=;
	printer_iterator(std::string delim = "\n") : m_delim{delim} {}
	template <typename T>
	void set(const T& arg) {
		std::cout << arg << m_delim;
	}

private:
	std::string m_delim;
};

class printer_joiner : public irul::output_iterator<printer_joiner> {
public:
	using output_iterator::operator=;
	printer_joiner(std::string delim = ", ") : m_delim{delim} {}
	template <typename T>
	void set(const T& arg) {
		if (m_first) {
			m_first = false;
		} else {
			std::cout << m_delim;
		}
		std::cout << arg;
	}

private:
	std::string m_delim;
	bool m_first = true;
};

int main() {
	std::copy(int_iterator{0}, int_iterator{5}, printer_iterator{});
	std::cout << "===============================\n";
	auto vec = std::vector<int>{2, 1, 5, 5, 2, 3, 7, 3};
	struct local_iterator : public irul::random_access_iterator<local_iterator, int> {
		local_iterator(std::vector<int>::iterator it, std::vector<int>& vec)
		        : m_it{it}, m_vec{&vec} {}
		int& get() const { return *m_it; }
		void advance(std::ptrdiff_t dist) { m_it += dist; }
		std::ptrdiff_t distance(local_iterator other) const {
			return std::distance(m_it, other.m_it);
		}
		std::size_t index() const {
			return static_cast<std::size_t>(std::distance(m_vec->begin(), m_it));
		}
		std::vector<int>::iterator m_it;
		std::vector<int>* m_vec;
	};
	auto it1 = local_iterator{vec.begin(), vec};
	it1[2] = 3;
	auto it2 = local_iterator{vec.end(), vec};
	std::sort(it1, it2);
	std::copy(vec.begin(), vec.end(), printer_iterator{});
	std::cout << "===============================\n";
	std::copy(it1, it2, printer_joiner{});
	std::cout << "\n===============================\n";
	auto i = 0u;
	for (; it1 < it2; ++it1) {
		std::cout << i++ << ": " << *it1 << '\n';
	}
}
