#ifndef IRUL_ITERATOR_HPP
#define IRUL_ITERATOR_HPP

#include <cstddef>
#include <iterator>
#include <utility>

namespace irul {

template <typename T>
struct temp_val {
	T value;
	T operator*() const { return value; }
	T* operator->() { return &value; }
	const T* operator->() const { return &value; }
};

template <typename Iter, typename ValueType, typename Distance = std::ptrdiff_t,
          typename Pointer = ValueType*, typename Reference = ValueType&>
class input_iterator {
public:
	using value_type = ValueType;
	using difference_type = Distance;
	using pointer = Pointer;
	using reference = Reference;
	using iterator_category = std::input_iterator_tag;

	reference operator*() const { return impl().get(); }
	pointer operator->() const { return &(impl().get()); }

	Iter& operator++() {
		impl().next();
		return impl();
	}
	temp_val<value_type> operator++(int) {
		temp_val<value_type> retval{impl().get()};
		impl().next();
		return retval;
	}

	friend bool operator==(const Iter& l, const Iter& r) { return l.equal(r); }
	friend bool operator!=(const Iter& l, const Iter& r) { return !(l == r); }

protected:
	Iter& impl() { return static_cast<Iter&>(*this); }
	const Iter& impl() const { return static_cast<const Iter&>(*this); }
};

template <typename Iter, typename ValueType, typename Distance = std::ptrdiff_t,
          typename Pointer = ValueType*, typename Reference = ValueType&>
class forward_iterator : public input_iterator<Iter, ValueType, Distance, Pointer, Reference> {
public:
	using iterator_category = std::forward_iterator_tag;

	using input_iterator<Iter, ValueType, Distance, Pointer, Reference>::operator++;
	Iter operator++(int) {
		auto temp = this->impl();
		this->impl().next();
		return temp;
	}
};

template <typename Iter, typename ValueType, typename Distance = std::ptrdiff_t,
          typename Pointer = ValueType*, typename Reference = ValueType&>
class bidirectional_iterator
        : public forward_iterator<Iter, ValueType, Distance, Pointer, Reference> {
public:
	using iterator_category = std::bidirectional_iterator_tag;
	Iter& operator--() {
		this->impl().prev();
		return this->impl();
	}
	Iter operator--(int) {
		auto temp = this->impl();
		this->impl().prev();
		return temp;
	}
};

template <typename Iter, typename ValueType, typename Distance = std::ptrdiff_t,
          typename Pointer = ValueType*, typename Reference = ValueType&>
class random_access_iterator
        : public bidirectional_iterator<Iter, ValueType, Distance, Pointer, Reference> {
public:
	using iterator_category = std::random_access_iterator_tag;
	using difference_type = Distance;
	using reference = Reference;

	// for convinience:
	void prev() { this->impl().advance(-1); }
	void next() { this->impl().advance(1); }
	bool equal(const Iter& other) const { return this->impl().distance(other) == 0; }

	Iter& operator+=(difference_type dist) {
		this->impl().advance(dist);
		return this->impl();
	}
	Iter& operator-=(difference_type dist) {
		this->impl().advance(-dist);
		return this->impl();
	}
	Iter operator+(difference_type dist) const {
		auto tmp = this->impl();
		tmp += dist;
		return tmp;
	}
	Iter operator-(difference_type dist) const {
		auto tmp = this->impl();
		tmp -= dist;
		return this->impl();
	}

	friend Iter operator+(difference_type dist, const Iter& it) { return it + dist; }
	reference operator[](difference_type dist) const { return *(this->impl() + dist); }

	difference_type operator-(const Iter& other) const { return other.distance(this->impl()); }

	bool operator<(const Iter& other) { return this->impl().distance(other) > 0; }
	bool operator>(const Iter& other) { return other < this->impl(); }
	bool operator<=(const Iter& other) { return !(other > this->impl()); }
	bool operator>=(const Iter& other) { return !(other < this->impl()); }
};

// While they are important, naked output-iterators behave very much
// unlike other iterators, so let's implement them outside the hierarchy:
template <typename Iter>
class output_iterator {
public:
	using value_type = void;
	using difference_type = void;
	using pointer = void;
	using reference = void;
	using iterator_category = std::output_iterator_tag;

	Iter& operator*() { return impl(); }
	Iter& operator++() { return impl(); }
	Iter& operator++(int) { return impl(); }

	template <typename T>
	Iter& operator=(T&& arg) {
		impl().set(std::forward<T>(arg));
		return impl();
	}

private:
	Iter& impl() { return static_cast<Iter&>(*this); }
	const Iter& impl() const { return static_cast<Iter&>(*this); }
};

} // namespace irul

# endif // include-guard