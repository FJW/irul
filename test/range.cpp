#include <vector>
#include <algorithm>

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

#include "irul/irul.hpp"

namespace {
	struct int_iterator: public irul::input_iterator<int_iterator, const int> {
		int_iterator(int val = 0): m_val{val} {}
		const int& get() const {return m_val;}
		void next() {++m_val;}
		bool equal(int_iterator other) const {return m_val == other.m_val;}
		int m_val;
	};
}

BOOST_AUTO_TEST_SUITE( Algorithm_tests )

BOOST_AUTO_TEST_CASE( basic_range ) {
	using int_range = irul::range<int_iterator>;
	auto rng = int_range{{2}, {5}};
	auto i = 2u;
	for(auto val: rng) {
		BOOST_CHECK_EQUAL(val, i++);
	}
}


BOOST_AUTO_TEST_SUITE_END()

